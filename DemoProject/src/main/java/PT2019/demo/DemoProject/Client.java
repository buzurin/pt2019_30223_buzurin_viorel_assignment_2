package PT2019.demo.DemoProject;

import java.util.Random;

public class Client {
	private int sosire;
	private int timpLucru;

	public Client(int s, int p) {
		this.sosire = s;
		this.timpLucru = p;
	}

	public int getSosire() {
		return this.sosire;
	}

	public int getLucru() {
		return this.timpLucru;
	}

	public void setLucru(int x) {
		this.timpLucru = x;
	}

	public int timpSosire(int intervalMaximSosire, int intervalMinim) {
		Random rand = new Random();
		return rand.nextInt((intervalMaximSosire - intervalMinim) + 1) + intervalMinim;
	}

	public int timpLucru(int timpServiciuMaxim, int timpServiciuMinim) {
		Random rand = new Random();
		this.timpLucru = rand.nextInt((timpServiciuMaxim - timpServiciuMinim) + 1) + timpServiciuMinim;
		return this.timpLucru;
	}

}
