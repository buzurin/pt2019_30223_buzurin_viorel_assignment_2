package PT2019.demo.DemoProject;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

public class Simulare {

	JPanel panel = new JPanel();
	JLabel[] lb = new JLabel[100];
	JLabel[] lbd = new JLabel[100];
	JLabel lb2 = new JLabel("0");
	JProgressBar[] progressBar = new JProgressBar[100];
	JTextField tf = new JTextField("10");
	JTextField tf1 = new JTextField("5");
	JTextField tf2 = new JTextField("1");
	JTextField tf3 = new JTextField("5");
	static JTextField tf4 = new JTextField("1");
	JTextField tf5 = new JTextField("4");
	JTextField tb1 = new JTextField("0");
	JTextField tb2 = new JTextField("0");
	JTextField tb3 = new JTextField("0");
	JTextField tb4 = new JTextField("0");
	JTextField tb5 = new JTextField("0");
	static Simulare sim = new Simulare();
	public static Coada[] c = new Coada[100];
	public static int numarCozi = 2, s = 0, l = 0, l3 = 0, interval = 50;

	public static void apel(int secunda, Coada[] a, int l1, int l2) {
		c = a;
		s = secunda;
		l = l1;
		l3 = l2;
	}

	class Threads implements Runnable {
		public boolean exit = false;

		public void run() {
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			while (!exit) {
				lb2.setText("" + s);
				for (int i = 0; i < numarCozi; i++) {
					String te = "";
					te = "Clienti la coada:" + c[i].clienti;
					progressBar[i].setValue((c[i].clienti * 10));
					lbd[i].setText(te);
				}
				if (s > interval) {
					double t = 0, t1 = 0;
					for (int j1 = 0; j1 < numarCozi; j1++) {
						t = t + c[j1].medieTimpAsteptare1;
						t1 = t1 + c[j1].medieTimpServiciu;
					}
					for (int i = 0; i < numarCozi; i++) {
						String te = "";
						te = "Timp in care coda este goala:" + c[i].coadaGoala;
						progressBar[i].setValue((c[i].coadaGoala));
						lbd[i].setText(te);
					}
					tb1.setText("" + c[0].oraDeVarf);
					tb2.setText("" + t1 / l);
					tb3.setText("" + t / l);
					tb4.setText("" + l);
					tb5.setText("" + l3);
					exit = true;
				}
			}
		}

		public void stop() {
			exit = true;
		}
	}

	public void init1() {
		panel.setLayout(new GridLayout(numarCozi, 3));
		for (int i = 0; i < 20; i++) {
			String s = "Coada" + (i + 1);
			lb[i] = new JLabel(s);
			lbd[i] = new JLabel("0");
			progressBar[i] = new JProgressBar();
			progressBar[i].setStringPainted(true);
		}
		for (int i = 0; i < numarCozi; i++) {
			panel.add(lb[i]);
			panel.add(progressBar[i]);
			panel.add(lbd[i]);
		}
	}

	public void init() {
		JFrame frame = new JFrame("Clienti");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 400);		
		JButton b1 = new JButton("Start");
		b1.addActionListener(new Start());
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JLabel lb1 = new JLabel("Secunde: ");
		JLabel l1 = new JLabel("Interval:");
		JLabel l2 = new JLabel("TimpMaximSosire:");
		JLabel l3 = new JLabel("TimpMinimSosire:");
		JLabel l4 = new JLabel("TimpMaximServire:");
		JLabel l5 = new JLabel("TimpMinimServire:");
		JLabel l6 = new JLabel("NumarCozi:");
		JLabel lbb1 = new JLabel("OraDeVarf");
		JLabel lbb2 = new JLabel("TimpSosieMediu");
		JLabel lbb3 = new JLabel("TimpAsteptareMediu");
		JLabel lbb4 = new JLabel("Persoane Plecate:");
		JLabel lbb5 = new JLabel("Persoane Venite");
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
		p1.add(l1);
		p1.add(tf);
		p1.add(l2);
		p1.add(tf1);
		p1.add(l3);
		p1.add(tf2);
		p1.add(l4);
		p1.add(tf3);
		p1.add(l5);
		p1.add(tf4);
		p1.add(l6);
		p1.add(tf5);
		p1.setLayout(new GridLayout(7, 2));
		p3.setLayout(new GridLayout(5, 2));
		p1.add(lb1);
		p1.add(lb2);
		p3.add(lbb1);
		p3.add(tb1);
		p3.add(lbb2);
		p3.add(tb2);
		p3.add(lbb3);
		p3.add(tb3);
		p3.add(lbb4);
		p3.add(tb4);
		p3.add(lbb5);
		p3.add(tb5);
		p2.add(p1);
		p2.add(p3);
		p2.add(b1);
		p.add(p2);
		p.add(panel);
		frame.add(p);
		frame.setVisible(true);
	}

	class Start implements ActionListener {
		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			for (int j = 0; j < numarCozi; j++) {
				c[j] = new Coada(0, 0);
			}
			int intervalMaximSosire = 5, intervalMinim = 1, timpServiciuMaxim = 5, timpServiciuMinim = 1, secunda = 0;
			numarCozi = Integer.parseInt(tf5.getText());
			interval = Integer.parseInt(tf.getText());
			intervalMaximSosire = Integer.parseInt(tf1.getText());
			intervalMinim = Integer.parseInt(tf2.getText());
			timpServiciuMaxim = Integer.parseInt(tf3.getText());
			timpServiciuMinim = Integer.parseInt(tf4.getText());
			Threads nr = new Threads();
			Thread one = new Thread(nr);
			sim.init1();
			Coada.functie(interval, intervalMaximSosire, intervalMinim, timpServiciuMaxim, timpServiciuMinim, secunda,
					numarCozi);
			s = 0;
			one.start();
			if (s == interval)
				one.stop();
			Coada a = new Coada(0, 0);
			a.Start();
		}
	}

	public static void main(String[] args) {

		sim.init();
	}
}